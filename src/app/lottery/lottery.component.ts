import {Component} from '@angular/core';

@Component({
  selector: 'app-lottery',
  templateUrl: './lottery.component.html',
  styleUrls: ['./lottery.component.css']
})
export class LotteryComponent {
  numberArray:number[] = [];
  showNumber = false;

  getRandomNumber(min: number, max: number){
    for(let i = 0; i < 5; i++){
      let randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
      this.numberArray.push(randomNumber);
    }
  }
  addRandomNumber(){
    this.showNumber = true;
    this.numberArray = [];
    this.getRandomNumber(5, 36)
    return this.numberArray.sort(function (a,b){
      return a-b;
    })
  }
}
