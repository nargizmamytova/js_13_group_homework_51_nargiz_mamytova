import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  password= '';
  imgTitle = '';
  imgURL = '';
  images = [
    {imgTitle: 'Cats', imgURl :'https://mirpozitiva.ru/wp-content/uploads/2019/11/1472042719_15.jpg'},
    {imgTitle: 'Yin Yang', imgURl :'https://static4.depositphotos.com/1000423/454/i/600/depositphotos_4548401-stock-photo-symbol-of-yin-and-yang.jpg'},
    {imgTitle: 'The ship', imgURl:'https://mobimg.b-cdn.net/v3/fetch/91/91e6d05dd40773292292bad5a75e75db.jpeg'},
  ]
  inputPassword(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.password = target.value;
  }
  showForm = false;
  addPassword(event: Event){
    event.preventDefault();
    if(this.password === 'origin'){
      this.showForm = true;
      this.reset();
    }else{
      alert('Add true password')
    }
  }
  reset(){
    this.password= '';
  }

  inputTitle(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.imgTitle = target.value;
  }
  inputURL(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.imgURL = target.value;
  }

  onAddImg(event:Event){
    event.preventDefault();
    this.images.push({
      imgTitle: this.imgTitle,
      imgURl: this.imgURL
    })

  }


}
